import argparse
import http.client
import json
import os

description = ('Download all Canvas conversations belonging to a '
               'particular account.')

parser = argparse.ArgumentParser(description=description)
parser.add_argument('base_url', help='Base URL of the Canvas host')
parser.add_argument('auth_token', help='Access token for user account')
args = parser.parse_args()

print('Retrieving conversation IDs...')

conversation_ids = []
page = 1
per_page = 100

client = http.client.HTTPSConnection(args.base_url)
while True:
    print(f'Retrieving {per_page} results from page {page}...')
    client.request('GET', f'/api/v1/conversations?page={page}&per_page={per_page}',
                   headers={'Authorization': f'Bearer {args.auth_token}'})
    body = json.load(client.getresponse())
    count = 0
    for message in body:
        conversation_ids.append(message['id'])
        count += 1
    print(f'Got total of {count} links.')
    if count != per_page:
        print('Done retrieving conversation IDs.')
        break
    else:
        page += 1

print('Retrieving and saving conversations...')

for conversation_id in conversation_ids:
    client.request('GET', f'/api/v1/conversations/{conversation_id}',
                   headers={'Authorization': f'Bearer {args.auth_token}'})
    body = json.load(client.getresponse())
    filename = str(conversation_id) + '.json'
    with open(filename, 'w') as f:
        json.dump(body, f)
        print(f'Saved conversation {conversation_id} to {filename}')

print('Done saving Canvas conversations.')
