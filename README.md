# Canvas Conversation Backup

This is a simple Python script which creates a local backup of a user's
conversations from Instructure's Canvas learning management system. Currently,
the Canvas Web UI does not provide such a function. The script communicates
with a Canvas server via the Canvas Web API. Each conversation is stored in a
JSON file in the given directory. To run the script you must use a
manually-generated authentication token, as described below.

## Usage

```
python convos_backup.py <server URL> <auth token> [destination directory]
```

## Authentication

You must manually create a Canvas authentication token and pass this token
into the script. You can do this from your Profile Settings page in the Canvas
Web UI. On the settings page is a section which allows you to create an access
token. Create a token, and then copy it into the script. For security, you
should delete this token from your account when the script is finished.
